const gulp = require('gulp');
const gulpStylelint = require('gulp-stylelint');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const minify = require('gulp-babel-minify');
const log = require('gulplog');
const tap = require('gulp-tap');
const browserify = require('browserify');
const buffer = require('gulp-buffer');
const esLint = require('gulp-eslint');
const svgmin = require('gulp-svgmin');
const size = require('gulp-filesize');

const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const notify = require("gulp-notify");
const path = require('path');



/* ----------------------------- CSS tasks ---------------------------- */
gulp.task('sass', () => {
    return gulp.src(['./src/sass/**/*.scss', '!./src/sass/**/_*.scss'])
        .pipe(gulpStylelint({
            reporters: [
                { formatter: 'string', console: true },
            ],
        }))
        // .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.init())
        .pipe(sass({ errLogToConsole: true }).on('error', sass.logError))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(size())
        .pipe(notify({
            message: 'SASS completed',
            onLast: true,
        }));
});


/* ----------------------------- JS tasks ----------------------------- */
gulp.task('js', () => {
    return gulp.src(['./src/js/**/*.js', '!./src/js/**/_*.js'], { read: false })
        // transform file objects using gulp-tap plugin
        .pipe(tap((file) => {

            log.info(`bundling ${file.path}`);

            // replace file contents with browserify's bundle stream
            file.contents = browserify(file.path, { debug: true }).bundle();

        }))

    // transform streaming contents into buffer contents
    // (because gulp-sourcemaps does not support streaming contents)
    .pipe(buffer())

    // load and init sourcemaps
    .pipe(sourcemaps.init({ loadMaps: true }))

    .pipe(babel({
            presets: ['@babel/preset-env'],
        }))
        .pipe(minify({
            mangle: {
                keepClassName: true,
            },
        }))

    // write sourcemaps
    .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(size());
});


gulp.task('esLint', () => {
    return gulp.src(['./src/js/**/*.js'])
        .pipe(esLint({
            configFile: './.eslintrc.js',
        }))
        .pipe(esLint.format());
});



/* -------------------------- WATCH, DEFAULT -------------------------- */

// Watch command
gulp.task('watch', () => {
    gulp.watch(['./src/sass/**/*.scss'], gulp.parallel('sass'));
    gulp.watch(['./src/js/**/*.js'], gulp.series('esLint', 'js'));
});

// Build command
gulp.task('build', gulp.series(gulp.parallel('sass', gulp.series('esLint', 'js')), 'watch'));

// Default
gulp.task('default', gulp.series('watch'));