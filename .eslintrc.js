module.exports = {
    "extends": "airbnb",
    "env": {
        "browser": true,
        "node": true,
        "es6": true,
        "jquery": true
    },
    "rules": {
        "arrow-body-style": ["error", "always"],
        "padded-blocks": 0,
        "one-var": 0,
        "indent" : ["error", 4],
        "max-len": ["error", { "code": 120 }]
    }
};
