<?php

/*
    Plugin Name: 10UP Primary Category
    Description: Custom primary category selector for posts.
    Version: 1.0.0
    Author: Edwil Jonas
    Text Domain: up-primary-category
*/

namespace UPC\Modules;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

// Define plugin constants
define( 'UPC_VERSION', time() );
define( 'UPC_HANDLE', 'upc_primary' );
define( 'UPC_ROOT', dirname( __FILE__ ) . DIRECTORY_SEPARATOR );
define( 'UPC_ASSETS', plugin_dir_url(__FILE__). 'assets' . DIRECTORY_SEPARATOR );
define( 'UPC_INC', UPC_ROOT. 'inc' . DIRECTORY_SEPARATOR );
define( 'UPC_MODULES', UPC_INC. 'modules' . DIRECTORY_SEPARATOR );
define( 'UPC_SETUP', UPC_INC. 'setup' . DIRECTORY_SEPARATOR );
define( 'UPC_LIB', UPC_INC. 'lib' . DIRECTORY_SEPARATOR );
define( 'UPC_DIST', plugin_dir_url( __FILE__ ). 'dist' . DIRECTORY_SEPARATOR );

// Classes
require_once( UPC_LIB . 'helpers.php' );

final class UPC_Modules {

    public function  __construct() {

        add_action( 'plugins_loaded', [ $this, 'include_functionality' ] );
        add_action( 'upc_modules_init', [ $this, 'init' ] );
    }

    public function init() {
        
        new UPC_Script_Manager();
        new UPC_Style_Manager();
        new UPC_Primary_Category();
        $backend = new UPC_Backend(); 
        new UPC_Ajax( $backend );        
    }

    public function include_functionality() {

        // Check requirements before executing
        if ( false === $this->meet_dependencies() ) {
            return;
        }

        // Setup Class Files
        include_once( UPC_SETUP . 'class-script-manager.php' );
        include_once( UPC_SETUP . 'class-style-manager.php' );

        // Module Class Files
        include_once( UPC_MODULES . 'class-backend.php' );
        include_once( UPC_MODULES . 'class-ajax.php' );
        include_once( UPC_MODULES . 'class-primary-category.php' );
        
        // Execute init
        do_action( 'upc_modules_init' );
    }

    /**
     * List here anything conditions this plugin
     * can not function without
     *
     * @return bool T
     */
    public function meet_dependencies() {
        
        return true;
    }
}

new UPC_Modules();