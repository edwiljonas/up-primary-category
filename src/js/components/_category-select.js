const categorySelect = {

    /**
     * Setup
     */
    init() {
        // Events
        this.events();
    },

    /**
     * Events
     */
    events() {
        this.getCategory();
    },
    injectHtml(data) {
        jQuery(data).each(function(index, element) {
            jQuery('[id="' + element.taxonomy + '"]').find('.selectit').each(function() {
                const id = jQuery(this).find('input').val();
                const is_checked = jQuery(this).find('input').is(':checked');
                jQuery(this).parent('li').find('.upc-category-select').remove();
                if (is_checked && element && element.value[0] === id) {
                    jQuery(this).before(' <span class="upc-category-select is-primary">Is Primary</span>');
                } else if (is_checked && element && element.value[0] !== id) {
                    jQuery(this).before(' <span class="upc-category-select">Make Primary</span>');
                }
            });
        });
        this.setCategory();
    },
    getCategory() {
        const id = jQuery('#post_ID').val();
        const self = this;
        jQuery.ajax({
            url: UPC.ajax_url,
            type: "POST",
            data: {
                'action': 'upc_get_category',
                'id': id
            },
            dataType: "json"
        }).done(function(data) {
            self.injectHtml(data);
            self.checkBox();
        }).fail(function(event) {});
    },
    setCategory() {
        const self = this;
        jQuery('.upc-category-select').off().on('click', function() {
            const id = jQuery('#post_ID').val();
            let value = jQuery(this).parent('li').find('input').val();
            const taxonomy = jQuery(this).parents('div[id*="taxonomy-"]').attr('id');
            if (jQuery(this).hasClass('is-primary')) {
                value = 0;
            }
            self.ajaxSet(id, value, taxonomy);
        });
    },
    checkBox() {
        const self = this;
        jQuery('[id*="taxonomy-"]').find('.selectit input').on('click', function() {
            if (!jQuery(this).is(':checked')) {
                // Check if is primary
                const selector = jQuery(this).parents('li').find('.upc-category-select');
                const id = jQuery('#post_ID').val();
                const taxonomy = jQuery(this).parents('div[id*="taxonomy-"]').attr('id');
                if (selector.hasClass('is-primary')) {
                    self.ajaxSet(id, 0, taxonomy);
                }
            }
            self.getCategory();
        });
    },
    ajaxSet(id, value, taxonomy) {
        const self = this;
        jQuery.ajax({
            url: UPC.ajax_url,
            type: "POST",
            data: {
                'action': 'upc_set_category',
                'id': id,
                'value': value,
                'taxonomy': taxonomy
            },
            dataType: "json"
        }).done(function(data) {
            self.injectHtml(data);
            const meta_value = jQuery('input[value="upc_primary-' + taxonomy + '"]').parents('tr').attr('id');
            jQuery('#' + meta_value).find('#' + meta_value + '-value').val(value);
        }).fail(function(event) {});
    }
};

module.exports = categorySelect;