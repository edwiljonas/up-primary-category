/*
 * Libs
 */
const Cookies = require('./libs/_cookies');

// Define global
if (typeof window.UPC == 'undefined') {
    window.UPC = {
        Cookies,
    };
} else {
    window.UPC.Cookies = Cookies;
}

/*
 * Components
 */
const categorySelect = require('./components/_category-select');

/*
 * On load
 */
jQuery(() => {
    // Category Select
    categorySelect.init();
});