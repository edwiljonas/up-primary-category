<?php

namespace UPC\Modules;

/**
 * Class for style manangement on the backend
 */
class UPC_Style_Manager {

    /**
     * Constructor
     */
    public function __construct() {

        if ( is_admin() ) :

            add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue' ) );
        endif;
    }    

    public function admin_enqueue() {

        wp_enqueue_style( 'upc-critical-css', UPC_DIST . 'css/critical.css', [], UPC_VERSION  );
	}
}
