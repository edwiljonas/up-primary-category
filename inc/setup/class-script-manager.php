<?php

namespace UPC\Modules;

/**
 * Class for script manangement on the backend
 */
class UPC_Script_Manager {

    /**
     * Constructor
     */
    public function __construct() {

        if ( is_admin() ) :

            add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue' ) );
        endif;
    }    

    public function admin_enqueue() {

        /**
         * Set global js varaible
         */
        wp_localize_script( 'jquery-core', 'UPC',
            array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
            )
        );

		wp_enqueue_script( 'upc-category-js', UPC_DIST . 'js/main.js', [ 'jquery' ], UPC_VERSION );
	}
}
