<?php

namespace UPC\Modules;

/**
 * Class for ajax hook setup
 */
class UPC_Ajax {

    /**
     * Varaibles
     */
	public $upc_ajax_backend_array;
	public $backend;

    /**
     * Constructor
     */
    public function __construct( $backend ) {
        
        $this->backend = $backend;  
        $this->upc_ajax_backend_array = $this->upc_get_backend_ajax_calls();      
        $this->upc_set_backend_ajax_calls();
    }    

    /**
     * Set hooks for backend
     */
    public function upc_set_backend_ajax_calls() {

        if( isset( $this->upc_ajax_backend_array ) && count( $this->upc_ajax_backend_array ) > 0 ){
			
			foreach( $this->upc_ajax_backend_array as $call ){

                add_action( 'wp_ajax_'. $call[ 'action' ], array( &$this->backend, $call['method'] ) );
            }
        }
    }

    /**
     * List of hooks to create for backend
     */
    public function upc_get_backend_ajax_calls() {

        $array = array(
            array( 'action' => 'upc_get_category', 'method' => 'upc_get_category' ),
            array( 'action' => 'upc_set_category', 'method' => 'upc_set_category' ),
        );

        return $array;
    }
}
