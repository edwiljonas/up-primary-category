<?php

namespace UPC\Modules;

/**
 * Class for ajax hook setup
 */
class UPC_Backend {

    /**
     * Constructor
     */
    public function __construct() {}    

    /**
     * Get primary category
     */
    public function upc_get_category() {

        // Variables
        $post_id = isset( $_POST[ 'id' ] ) ? $_POST[ 'id' ] : null;
        $categories = [];

        if ( $post_id !== null ) {

            $taxonomies = get_post_taxonomies( $post_id );

            foreach ( $taxonomies as $tax ) {

                array_push( $categories, [
                    'taxonomy' => 'taxonomy-' . $tax,
                    'value' => get_post_meta( $post_id, UPC_HANDLE . '-taxonomy-' . $tax )
                ] );
            }
        }

        echo json_encode( $categories );
        exit();
    }

    /**
     * Set hooks for backend
     */
    public function upc_set_category() {

        // Variables
        $post_id = isset( $_POST[ 'id' ] ) ? $_POST[ 'id' ] : null;
        $value = isset( $_POST[ 'value' ] ) ?$_POST[ 'value' ] : null;
        $taxonomy = isset( $_POST[ 'taxonomy' ] ) ?$_POST[ 'taxonomy' ] : null;
        $categories = [];

        if ( $post_id !== null && $value !== null && $taxonomy !== null ) {

            if ( ! add_post_meta( $post_id, UPC_HANDLE . '-' . $taxonomy, $value, true ) ) {   
                          
                update_post_meta ( $post_id, UPC_HANDLE . '-' . $taxonomy, $value );
            }

            array_push( $categories, [
                'taxonomy' => $taxonomy,
                'value' => get_post_meta( $post_id, UPC_HANDLE . '-' . $taxonomy )
            ] );

            echo json_encode( $categories );
            exit();
        }

        echo json_encode( false );
        exit();
    }
}
