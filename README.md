# README #

Primary category selector for categories.

# Query posts by category ID #

```php
$args = array(
	'numberposts' => -1,
    'post_type' => 'post',
	'orderby' => 'post_title',
	'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => 'upc_primary-taxonomy-category',
            'value' => $category_ID,
        )
    )
);

$posts = get_posts( $args );
```

# If there are multiple category selectors on a edit screen, the key for each category will be generated as follows: #

```php
/**
 *  Countries
 *  http://domain/wp-admin/edit-tags.php?taxonomy=countries&post_type=payment-method
 */
array(
    'key' => 'upc_primary-taxonomy-countries',
    'value' => $category_ID,
)

/**
 *  Regions
 *  http://domain/wp-admin/edit-tags.php?taxonomy=regions&post_type=payment-method
 */
array(
    'key' => 'upc_primary-taxonomy-regions',
    'value' => $category_ID,
)

/**
 *  Example {TAXONOMY_NAME}
 *  http://domain/wp-admin/edit-tags.php?taxonomy=TAXONOMY_NAME&post_type=payment-method
 */
array(
    'key' => 'upc_primary-taxonomy-TAXONOMY_NAME',
    'value' => $category_ID,
)
```